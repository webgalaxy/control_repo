# Puppet Notes
## Puppet Resources
#### file
#### user
#### service
#### package

## Intallation & configuration -
* *Below will run on Master and Node both*
#### Intall and configure NTP
```shell
yum install ntp ntpdate 
ntpdate 0.centos.pool.ntp.org 
systemctl start ntpd 
systemctl enable ntpd
```

#### add puppet repo
```shell
rpm -Uvh https://yum.puppet.com/puppet6-release-el-7.noarch.rpm
```
* *Below will run on Master*
```shell
yum install puppetserver
vim /etc/sysconfig/puppetserver
# Modify this if you'd like to change the memory allocation, enable JMX, etc
JAVA_ARGS="-Xms512m -Xmx512m -Djruby.logger.class=com.puppetlabs.jruby_utils.jruby.Slf4jLogger"

vim /etc/puppetlabs/puppet/puppet.conf
[main]
certname = master.test.local
server = master.test.local
environment = production
runinterval = 1h

systemctl start puppetserver
systemctl enable puppetserver
firewall-cmd --add-port=8140/tcp --permanent
firewall-cmd --reload
vim .bash_profile
PATH=$PATH:/opt/puppetlabs/puppet/bin

```

* *Below will run on Node*
```shell
yum install puppet-agent
vim /etc/puppetlabs/puppet/puppet.conf
[main]
certname = node1.test.local
server = master.test.local
environment = production
runinterval = 1h


/opt/puppetlabs/bin/puppet agent --test
```
* *sign agent certificate on Master*
```shell
/opt/puppetlabs/server/apps/puppetserver/bin/puppetserver ca list
/opt/puppetlabs/server/apps/puppetserver/bin/puppetserver ca sign --certname node1.test.local
```

#### Setup r10k 
```shell
/opt/puppetlabs/puppet/bin/gem install r10k
mkdir /etc/puppetlabs/r10k
vim /etc/puppetlabs/r10k/r10k.yaml
---
 :cachedir: '/var/cache/r10k'
 :sources:
         :my-org:
                remote: 'https://gitlab.com/webgalaxy/control_repo.git'
                basedir: '/etc/puppetlabs/code/environments'


r10k deploy environment -p
```




